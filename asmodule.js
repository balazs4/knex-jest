module.exports = () => {
  const knex = require('knex')({
    dialect: 'sqlite3',
    connection: {
      filename: './data.db'
    }
  });
  return knex
    .select('*')
    .from('users')
    .where({ name: 'Tim' });
};
