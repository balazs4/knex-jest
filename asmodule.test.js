const query = require('./asmodule');

jest.mock('knex', () => {
  return cfg => {
    return {
      select: selectarg => {
        return {
          from: fromarg => {
            return {
              where: wherearg => {
                return Promise.resolve(['my', 'mock', 'result']);
              }
            };
          }
        };
      }
    };
  };
});

it('mock as module works', () => {
  return query().then(result => {
    expect(result).toEqual(['my', 'mock', 'result']);
  });
});
