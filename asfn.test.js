const query = require('./asfn');

it('mock as function works', () => {
  const knex = {
    select: jest.fn(selectarg => {
      return {
        from: fromarg => {
          return {
            where: wherearg => {
              return Promise.resolve(['my', 'mock', 'result']);
            }
          };
        }
      };
    })
  };
  return query(knex)
    .then(result => {
      expect(result).toEqual(['my', 'mock', 'result']);
    })
    .then(_ => expect(knex.select).toBeCalledWith('*'));
});
